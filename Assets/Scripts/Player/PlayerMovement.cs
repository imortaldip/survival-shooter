﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public float speed = 6f;  //Player got the default floating point variable

	Vector3 movement;
	Animator anim;
	Rigidbody playerRigidbody;
	int floorMask;
	float camRayLength = 100f;

	void Awake() //similar to Start(), but its called regardless of the script is emable or not
	{
		floorMask = LayerMask.GetMask ("Floor"); //get the Layer Mask from the Quad Floor
		anim = GetComponent<Animator> ();
		playerRigidbody = GetComponent<Rigidbody> ();
	}

	void FixedUpdate() //Unity automatically calls this function in a scripts that finds the Physics Objects or Rigidbody attached
	{
		float h = Input.GetAxisRaw ("Horizontal"); //Unlike a standard input (axis value varying between -1 and 1), the Raw input only have 
		float v = Input.GetAxisRaw ("Vertical");  // only value of -1, 0 or 1
												// it helps character rather than slowing speeding upto full speed, it grabs full speed at once
		Move (h, v);
		Turning ();
		Animating (h, v);
	}

	void Move(float h, float v)
	{
		movement.Set (h, 0f, v);
		movement = movement.normalized * speed * Time.deltaTime;
		//normalize the speed in the diagonal,deltaTime is the time between each update called
		playerRigidbody.MovePosition (transform.position + movement);
	}

	void Turning()
	{
		Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition); //Ray comming from the camera, which finds the point underneath the mouse
		RaycastHit floorHit;					//cast the imaginary ray from the camera
		if(Physics.Raycast (camRay, out floorHit, camRayLength, floorMask)) //if the ray hits something
		{
			Vector3 playerToMouse = floorHit.point - transform.position;
			playerToMouse.y = 0f;

			Quaternion newRotation = Quaternion.LookRotation (playerToMouse);//default forward axis (Z) is giving player to mouse 
			playerRigidbody.MoveRotation (newRotation);
		} 
	}

	void Animating(float h, float v)
	{
		bool walking = h != 0f || v != 0f;
		anim.SetBool ("IsWalking", walking);
	}
    
}
